package inf.eureka.tarsier.rest

import org.specs2.mutable.Specification
import net.liftweb.json.{JsonParser, DefaultFormats}

/**
 * User: colt44
 * Date: 8/23/12
 * Time: 10:30 PM
 */
class TaskRestSpec extends Specification {
  implicit val formats = DefaultFormats

  "task service inf.eureka.tarsier.rest" should {
    "parse tasks without tags" in {
      val noTags = JsonParser.parse(
        """
          {
            "task":"new test",
            "start":"undefined",
            "end":"undefined",
            "tags":[]
          }
        """)

      val task = noTags.extract[TaskJson]

      task.task must be equalTo "new test"
    }
    "but it should also parse tasks with tags" in {
      val noTags = JsonParser.parse(
        """
          {
            "task":"new test",
            "start":"undefined",
            "end":"undefined",
            "tags":[{"tag": "#tag1"}]
          }
        """)

      val task = noTags.extract[TaskJson]

      "task should be 'new test'" >> {task.task must be equalTo "new test"}
      "tag should be '#tag1" >> {task.tags.head.tag must be equalTo "#tag1"}
    }
    "and parse tasks with multiple tags" in {
      val noTags = JsonParser.parse(
        """
          {
            "task":"new test",
            "start":"undefined",
            "end":"undefined",
            "tags":[{"tag": "#tag1"}, {"tag": "#tag2"}]
          }
        """)

      val task = noTags.extract[TaskJson]

      "task should be 'new test" >> {task.task must be equalTo "new test"}
      "first tag should be '#tag1'" >> {task.tags.head.tag must be equalTo "#tag1"}
      "second tag should be '#tag2'" >> {task.tags.tail.head.tag must be equalTo "#tag2"}
    }
  }
}
