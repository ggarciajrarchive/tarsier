package inf.eureka.tarsier.lib

import org.specs2.mutable.Specification
import java.util.Calendar
import net.liftweb.common.Full

/**
 * User: colt44
 * Date: 8/23/12
 * Time: 10:30 PM
 */
class FormatterSpec extends Specification {
  "formatter utils in inf.eureka.tarsier.rest" should {
    "format dates and time" in {
      val cal1 = Calendar.getInstance()
      cal1.set(2012, 7, 25, 0, 0, 0)

      "Calendar(2012,7,25,0,0,0) should be formated to 25/08/2012" >> {
        Formatter.format(Full(cal1.getTime), "dd/MM/yyyy") must be equalTo ("25/08/2012")
      }
      "Calendar(2012,7,250,0,0,0) should be formated to 25/08/2012 00:00:00" >> {
        Formatter.format(Full(cal1.getTime), "dd/MM/yyyy HH:mm:ss") must be equalTo ("25/08/2012 00:00:00")
      }

      val cal2 = Calendar.getInstance()
      cal2.set(2012, 7, 25, 23, 59, 59)

      "Calendar(2012,7,25,23,59,59) should be formated to 25/08/2012" >> {
        Formatter.format(Full(cal2.getTime), "dd/MM/yyyy") must be equalTo ("25/08/2012")
      }
      "Calendar(2012,7,25,23,59,59) should be formated to 25/08/2012 23:59:59" >> {
        Formatter.format(Full(cal2.getTime), "dd/MM/yyyy HH:mm:ss") must be equalTo ("25/08/2012 23:59:59")
      }
    }
    "convert strings into dates" in {
      val dt1 = Formatter.parse(Full("25/08/2012 00:00:00"), "dd/MM/yyyy HH:mm:ss")
      val dt2 = Formatter.parse(Full("25/08/2012 23:59:59"), "dd/MM/yyyy HH:mm:ss")

      "converted date 25/08/2012 00:00:00 using format dd/MM/yyyy should be 25/08/2012" >> {
        Formatter.format(Full(dt1), "dd/MM/yyyy") must be equalTo ("25/08/2012")
      }
      "converted date 25/08/2012 00:00:00 using format dd/MM/yyyy HH:mm:ss should be 25/08/2012 00:00:00" >> {
        Formatter.format(Full(dt1), "dd/MM/yyyy HH:mm:ss") must be equalTo ("25/08/2012 00:00:00")
      }
      "converted date 25/08/2012 23:59:59 using format dd/MM/yyyy should be 25/08/2012" >> {
        Formatter.format(Full(dt2), "dd/MM/yyyy") must be equalTo ("25/08/2012")
      }
      "converted date 25/08/2012 23:59:59 using format dd/MM/yyyy HH:mm:ss should be 25/08/2012 23:59:59" >> {
        Formatter.format(Full(dt2), "dd/MM/yyyy HH:mm:ss") must be equalTo ("25/08/2012 23:59:59")
      }
    }
    "convert long representing time into a formatted string" in {
      "60 should be 00:01:00" >> { Formatter.format(60) must be equalTo ("00:01:00") }
      "360 should be 00:06:00" >> { Formatter.format(360) must be equalTo ("00:06:00") }
      "3960 should be 01:06:00" >> { Formatter.format(3960) must be equalTo ("01:06:00") }
      "54 should be 00:00:54" >> { Formatter.format(54) must be equalTo ("00:00:54") }
    }
  }
}
