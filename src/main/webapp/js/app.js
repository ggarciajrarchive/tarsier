define([
    'underscore',
    'views/form',
    'views/list',
    'views/search',
    'views/last_days',
    'views/earlier',
    'views/add_activity_modal',
    'views/edit_delete',
    'views/stack',
    'collections/days',
    'models/state',
    'modules/tags_operations'
], function (_, CreateTaskView, ListTasksView, SearchView, LastDaysView, AddEarlierView, AddEarlierModalView,
             DeleteEditModalView, StackView, Days, State, TagsOperation) {

    var _executeAndUpdate = function(func, counter) {
        func();
        $(".bar").css("width", counter);
    };

    var initialize = function () {
        $(".bar").css("width", "0%");

        _executeAndUpdate(function() {
            State.set("tags", TagsOperation.fetchAll());
        }, "10%");

        var days = new Days();
        var lastDays = new Days();

        _executeAndUpdate(function() {
            State.set({"days": days});
            State.set({"lastDays": lastDays});
            State.set({"createTaskView": new CreateTaskView()});
        }, "15%");

        _executeAndUpdate(function() {
            State.set({"listView": new ListTasksView()});
        }, "30%");

        _executeAndUpdate(function() {
            State.set({"searchView": new SearchView()});
        }, "35%");

        _executeAndUpdate(function() {
            State.set({"lastDaysView": new LastDaysView()});
        }, "40%");

        _executeAndUpdate(function() {
            State.set({"addEarlierViewsearchView": new AddEarlierView()});
        }, "45%");

        _executeAndUpdate(function() {
            State.set({"addEarlierModalView": new AddEarlierModalView()});
        }, "50%");

        _executeAndUpdate(function() {
            State.set({"deleteEditModalView": new DeleteEditModalView()});
        }, "60%");

        _executeAndUpdate(function() {
            days.bind('add', State.get("listView").render);
            days.bind('reset', State.get("listView").render);

        }, "70%");

        _executeAndUpdate(function() {
            days.bind('change', State.get("listView").render);
            days.bind('add', State.get("searchView").updateTotals);
        }, "80%");

        _executeAndUpdate(function() {
            days.bind('change', State.get("searchView").updateTotals);
            lastDays.bind('add', State.get("lastDaysView").updateTotals);
        }, "85%");

        _executeAndUpdate(function() {
            State.set({"stackView": new StackView()});
        }, "90%");

        _executeAndUpdate(function() {
            lastDays.bind('change', State.get("lastDaysView").updateTotals);
        }, "100%");

        $("#loading").hide();
        $(".main-content").show();
    };

    return {
        initialize:initialize
    };
});


