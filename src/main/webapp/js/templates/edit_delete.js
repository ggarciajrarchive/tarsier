<form action="#" class="form-search">
    <fieldset>
        <legend>Edit / Delete task</legend>
        <div class="alert alert-error">teste</div>
        <div class="control-group">
            <label for="edit-activity" class="control-label">Activity</label>
            <input type="text" id="edit-activity" class="input-xxxlarge" value="<%= t.get("task") %> <%= t.tagsToString() %>"/>
        </div>

        <div class="control-group form-from-until">
            <label for="edit-calendar-from" class="control-label">From:</label>
            <input type="text" id="edit-calendar-from" class="input-medium" value="<%= t.get("start") %>"/>
            <span class="spacer1">&nbsp;</span>
            <label for="edit-calendar-until" class="control-label">Until:</label>
            <input type="text" id="edit-calendar-until" class="input-medium" value="<%= t.get("end") || "" %>"/>
        </div>

        <div class="form-actions">
            <input type="hidden" id="tid" value="<%= t.get("id") %>"/>
            <input type="hidden" id="day" value="<%= day %>"/>
            <button type="submit" class="reopen-task btn btn-primary"><span class="icon-refresh icon-white"></span>Re-open</button>
            <button type="submit" class="edit-task btn btn-primary"><span class="icon-edit icon-white"></span>Save</button>
            <button type="submit" class="delete-task btn btn-danger"><span class="icon-trash icon-white"></span>Delete</button>
            <button type="submit" class="close-modal btn"><span class="icon-remove"></span>Close Dialog</button>
        </div>
    </fieldset>
</form>