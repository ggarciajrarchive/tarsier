<form action="#" class="form-search">
    <fieldset>
        <legend>Add earlier task</legend>
        <div class="alert alert-error"></div>
        <div class="control-group">
            <label for="earlier-activity" class="control-label">Activity</label>
            <input type="text" id="earlier-activity" class="input-xxxlarge"/>
        </div>

        <div class="control-group form-from-until">
            <label for="earlier-calendar-from" class="control-label">From:</label>
            <input type="text" id="earlier-calendar-from" class="input-medium"/>
            <span class="spacer1">&nbsp;</span>
            <label for="earlier-calendar-until" class="control-label">Until:</label>
            <input type="text" id="earlier-calendar-until" class="input-medium"/>
        </div>

        <div class="form-actions">
            <button type="submit" class="create-earlier-task btn btn-primary"><span class="icon-plus-sign icon-white"></span>Add</button>
            <button type="submit" class="close-earlier-modal btn"><span class="icon-remove"></span>Close</button>
        </div>
    </fieldset>
</form>