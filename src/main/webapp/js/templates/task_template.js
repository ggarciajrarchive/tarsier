<% _.each(days, function(d) { %>
    <div class="row">
        <div class="span7"><h3><%= moment(d.get("day").toString(), "YYYYMMDD").format("dddd, MMMM DD") %></h3></div>
        <div class="span1"><h3 class="pull-right"><%= d.get("totals") %></h3></div>
            <table class="span8 table">
            <tbody class="table-bordered">
            <% _.each(d.get("tasks").models, function(t) { %>
            <tr class="pointer" data-id="<%= t.get("id") %>" data-day="<%= d.get("day") %>">
                <td class="task-content span9"><%= t.get("task") %>
                &nbsp;
                <% if (t.get("tags").length > 0) { %>
                    <% _.each(t.get("tags"), function(tg) { %>
                      <span class="tag"><% if (tg.tag.indexOf('#') < 0) { %>#<% } %><%= tg.tag %></span>
                    <% }) %>
                <% } %>
                </td>
                <td class="span1"><span class="pull-right"><%= t.get("start").split(" ")[1] %></span></td>
                <td class="span1">
                    <% if (t.get("end") && t.get("end") != "") { %>
                    <span class="pull-right">
                        <%= t.get("end").split(" ")[1] %>
                    </span>
                    <% } %>
                </td>
            </tr>
            <% }) %>
            </tbody>
            </table>
        </div>
    </div>
<% }) %>