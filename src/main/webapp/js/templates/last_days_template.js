<% _.each(days, function(d) { %>
<ul>
    <li>
        <h3><%= moment(d.get("day").toString(), "YYYYMMDD").format("dddd, MMMM DD") %> (<span class="tag"><%= d.get("totals") %></span>)</h3>

        <ul>
            <% _.each(d.get("tasks").models, function(t) { %>
            <li>
            <span>
                <%= t.get("start").split(" ")[1] %>
                <% if (t.get("end") && t.get("end") != "") { %>
                     - <%= t.get("end").split(" ")[1] %>
                <% } %>
            </span>

             &raquo; <%= t.get("task") %>

            <% if (t.get("tags").length > 0) { %>
                <% _.each(t.get("tags"), function(tg) { %>
                  <span class="tag"><% if (tg.tag.indexOf('#') < 0) { %>#<% } %><%= tg.tag %></span>
                <% }) %>
            <% } %>
            </li>
            <% }) %>
        </ul>
    </li>
</ul>
<% }) %>