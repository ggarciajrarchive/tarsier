require.config({
    baseUrl: '/js',
    paths: {
        text: 'libs/require/require.text.2.0.3',
        jQuery: 'libs/jquery/jquery.1.8.0.min',
        jqueryui: 'libs/jquery/jquery-ui-1.8.23.custom.min',
        jquerytimpicker: 'libs/jquery/jquery.timepicker',
        underscore: 'libs/underscore/underscore.1.3.3.min',
        backbone: 'libs/backbone/backbone.0.9.2.min',
        moment: 'libs/moment/moment.1.7.2',
        fancybox: 'libs/fancybox/jquery.fancybox.pack',
        operation: 'modules/task_operations',
        tinyscroll: 'libs/tinyscrollbar/tinyscrollbar-1.81'
    },
    shim: {
        'libs/bootstrap/bootstrap.min': ['jQuery'],
        'jqueryui': ['jQuery'],
        'jquerytimpicker': ['jqueryui', 'jQuery'],
        'fancybox': ['jQuery'],
        'backbone': {
            deps: ['underscore', 'jQuery'],
            exports: 'Backbone'
        },
        'underscore': {
            exports: "_"
        },
        'jQuery': {
            exports: "jQuery"
        }
    }
});

require([
    'app'
], function(App){
    App.initialize();
});