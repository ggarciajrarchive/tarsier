define([
    'jquery',
    'moment'
], function ($, moment) {
    var startLessThanEnd = function(start, end) {
        var valid = true;

        if (end) {
            var s = moment(start, "DD/MM/YYYY HH:mm:ss");
            var e = moment(end, "DD/MM/YYYY HH:mm:ss");

            valid = e.diff(s) > 0;
        }

        return valid;
    };

    return {
        startLessThanEnd:startLessThanEnd
    };
});