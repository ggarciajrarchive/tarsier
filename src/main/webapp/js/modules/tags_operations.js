define([
    'jquery',
    'collections/tags',
    'models/state',
    'models/tag'
], function ($, Tags, State, Tag) {
    var fetchAll = function() {
        var tags = {};

        $.ajax({
            url:'/api/tags.json',
            dataType:"json",
            type:'get',
            async: false,
            success:function (data) {
                tags = new Tags(data.tags);
            },
            error:function (data) {
                console.log(data);
            }
        });

        return tags;
    };

    var add = function(rawTask) {
        var tagsTok = _.filter(rawTask.split(" "), function (e) {
            return e.match("^#")
        });

        _.each(tagsTok, function(t) {
            var arr = State.get("tags").where({tag:t.substring(1)});

            if (arr.length == 0)
                State.get("tags").add({tag:t.substring(1)});
        });
    };

    var appendTag = function(tag, ele) {
        var curVal = $(ele).val();
        var lastChars = curVal.split(' ').reverse()[0];
        var choppedCurVal = curVal.substring(0, curVal.length-lastChars.length);

        $(ele).val(choppedCurVal + "#" + tag);
    };

    return {
        fetchAll:fetchAll,
        add:add,
        appendTag:appendTag
    };
});