define([
    'jquery',
    'underscore',
    'models/day',
    'models/tag',
    'collections/tags',
    'models/task',
    'models/state',
    'collections/tasks',
    'moment'
], function ($, _, Day, Tag, Tags, Task, State, Tasks, moment) {
    var _createTask = function (rawTask) {
        var tokens = rawTask.split(" ");

        var tagsTok = _.filter(tokens, function (e) {
            return e.match("^#")
        });

        var ts = _.filter(tokens, function (e) {
            return !e.match("^#")
        });

        var t = "";

        _.each(ts, function (e) {
            if (t.length > 0)
                t += " " + e;
            else
                t += e;
        });

        var tags = new Tags;

        _.each(tagsTok, function (e) {
            tags.add(new Tag({tag:e}));
        });

        return new Task({
            task:t.trim(),
            tags:tags
        });
    };

    var _getDay = function(day) {
        return _.find(State.get("days").models, function(d) { return d.get("day") == day});
    };

    var _updateDays = function(day, task, operation) {
        var d = _getDay(day);
        d.get("tasks").remove(task);

        if (operation != "delete")
            d.get("tasks").add(task);

        if (d.get("tasks").length == 0)
            State.get("days").remove(d);

        State.get("days").trigger("add");
    };

    var calculateTotal = function(days) {
        var diff = 0;

        _.each(days, function(d) {
            _.each(d.get("tasks").models, function(t) {
                if (t.get("start") && t.get("end")) {
                    var start = moment(t.get("start"), "DD/MM/YYYY HH:mm:ss");
                    var end = moment(t.get("end"), "DD/MM/YYYY HH:mm:ss");

                    diff += end.diff(start, 'seconds');
                }
            });
        });

        var hours = parseInt(diff / 3600);
        var remainder = parseInt(diff % 3600);
        var minutes = parseInt(remainder / 60);
        var seconds = parseInt(remainder % 60);

        return ((hours < 10) ? "0" : "") + hours + ":" +
            ((minutes < 10) ? "0" : "") + minutes + ":" +
            ((seconds < 10) ? "0" : "") + seconds;
    };

    var calculateTotalByDay = function(tasks) {
        var diff = 0;

        _.each(tasks, function(t) {
            if (t.get("start") && t.get("end")) {
                var start = moment(t.get("start"), "DD/MM/YYYY HH:mm:ss");
                var end = moment(t.get("end"), "DD/MM/YYYY HH:mm:ss");

                diff += end.diff(start, 'seconds');
            }
        });

        var hours = parseInt(diff / 3600);
        var remainder = parseInt(diff % 3600);
        var minutes = parseInt(remainder / 60);
        var seconds = parseInt(remainder % 60);

        return ((hours < 10) ? "0" : "") + hours + ":" +
            ((minutes < 10) ? "0" : "") + minutes + ":" +
            ((seconds < 10) ? "0" : "") + seconds;
    };

    var tasksByDate = function(date) {
        return _.filter(State.get("days").models, function (d) {
            return d.get("day") == parseInt(date);
        })[0];
    };

    var addWithDate = function(content, start, end) {
        var task = _createTask(content);
        if (start) {
            task.set({"start": start});
        }

        if (end) {
            task.set({"end": end});
        }

        task.save(null, {
            async:false,
            error:function (data) {
                console.log(data);
            },
            success:function (data) {
                var lastRunning = data.get("lastRunning");
                var lastAdded = data.get("lastAdded");

                if (lastRunning) {
                    var lr = running();

                    if (lr) {
                        lr.set({"end":lastRunning.end});
                    }
                }

                var day = tasksByDate(
                    moment(lastAdded.start, "DD/MM/YYYY HH:mm:ss").format("YYYYMMDD")
                );

                if (day) {
                    day.get("tasks").add(lastAdded);

                    State.get("days").trigger("change");
                } else {
                    State.get("days").add(
                        new Day({
                            "day" : moment(lastAdded.start, "DD/MM/YYYY HH:mm:ss").format("YYYYMMDD"),
                            "tasks" : new Tasks(lastAdded)
                        })
                    );
                }
            }
        });
    };

    var add = function (content) {
        addWithDate(content, moment().format("DD/MM/YYYY HH:mm:ss"), null);
    };

    var editWithDate = function(day, id, content, start, end) {
        var task = find(day, id);

        var t = _createTask(content);

        task.set({
            "task":t.get("task"),
            "start": start,
            "tags":t.get("tags")
        });

        if (end && end != "") {
            task.set({"end": end})
        } else {
            task.set({"end": null})
        }

        task.save(null, {
            async:false,
            method: 'PUT',
            contentType: 'application/json',
            url: '/api/tasks.json',
            error:function (data) {
                console.log(data);
            },
            success:function (data) {
                _updateDays(day, task, 'update');
            }
        });
    };

    var deleteTask = function(day, id) {
        var task = find(day, id);
        var url = window.location.protocol + "//" +
            window.location.host + "/api/tasks/" + id +
            "/delete.json";

        $.ajax({
            async:false,
            url: url,
            error:function (data) {
                console.log(data);
            },
            success:function (data) {
                _updateDays(day, task, 'delete');
            }
        });
    };

    var find = function (day, id) {
        var d = _getDay(day);

        return (d)
            ? _.find(d.get("tasks").models, function(t){ return t.get("id") == id; })
            : null;
    };

    var running = function() {
        var running;
        var day = tasksByDate(moment().format("YYYYMMDD")) || State.get("days").models.reverse()[0];

        if (day) {
            var lr = _.filter(day.get("tasks").models, function (t) {
                return t.get("end") == "";
            })[0];

            if (lr) {
                running = lr;
            }
        }

        return running;
    };

    var stop = function () {
        var selectedTask = running();
        var day = moment(selectedTask.get("start"), "DD/MM/YYYY HH:mm:ss").format("YYYYMMDD");

        //stop is an special case of update. just need to update task's end date.
        editWithDate(
            day,
            selectedTask.get("id"),
            selectedTask.get("task") + " " + selectedTask.tagsToString(),
            selectedTask.get("start"),
            moment().format("DD/MM/YYYY HH:mm")
        )
    };

    var lastDaysList = function () {
        var start = moment().subtract('days', 5).format("DD-MM-YYYY");
        var end = moment().add('days', 1).format("DD-MM-YYYY");

        $.ajax({
            url:'/api/tasks/' + start + '/' + end + '/list.json',
            dataType:"json",
            type:'get',
            success:function (data) {
                State.get("lastDays").reset();
                var lastDays = State.get("lastDays");

                _.each(data.days, function (d) {
                    var tasks = new Tasks(d.tasks);

                    var day = new Day({
                        "day":d.day,
                        "totals": calculateTotalByDay(tasks.models),
                        "tasks": tasks
                    });

                    lastDays.add(day);
                });
            },
            error:function (data) {
                console.log(data);
            }
        });
    };

    var list = function (start, end, tags) {
        var lTags = "";

        if (tags) {
            _.each(tags.split(" "), function(t) {
                if (lTags.length > 0)
                    lTags += ",";

                lTags += t.substr(1);
            });
        }

        $.ajax({
            url:'/api/tasks/' + start + '/' + end + '/list.json?tags=' + lTags,
            dataType:"json",
            type:'get',
            success:function (data) {
                State.get("days").reset();
                var days = State.get("days");

                _.each(data.days, function (d) {
                    var tasks = new Tasks(d.tasks);

                    var day = new Day({
                        "day":d.day,
                        "totals": calculateTotalByDay(tasks.models),
                        "tasks": tasks
                    });

                    days.add(day);
                });
            },
            error:function (data) {
                console.log(data);
            }
        });
    };

    return {
        add:add,
        addWithDate:addWithDate,
        editWithDate:editWithDate,
        deleteTask:deleteTask,
        stop:stop,
        list:list,
        lastDaysList:lastDaysList,
        running:running,
        calculateTotalByDay:calculateTotalByDay,
        calculateTotal:calculateTotal,
        find:find
    };
});