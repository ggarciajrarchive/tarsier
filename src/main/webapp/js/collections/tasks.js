define([
    'backbone',
    'models/task'
], function (Backbone, Task) {
    return Backbone.Collection.extend({
        comparator: function(ele) {
            return ele.get("start");
        },

        model:Task
    });
});