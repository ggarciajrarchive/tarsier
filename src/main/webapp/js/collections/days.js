define([
    'backbone',
    'models/day'
], function (Backbone, Day) {
    return Backbone.Collection.extend({
        comparator: function(ele) {
            return ele.get("day");
        },

        model:Day
    });
});