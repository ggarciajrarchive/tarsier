define([
    'backbone',
    'models/task'
], function (Backbone, Task) {
    return Backbone.Collection.extend({
        model:Task
    });
});