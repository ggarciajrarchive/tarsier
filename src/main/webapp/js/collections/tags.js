define([
    'backbone',
    'models/tag'
], function (Backbone, Tag) {
    return Backbone.Collection.extend({
        model:Tag
    });
});