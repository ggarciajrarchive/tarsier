define([
    'backbone',
    'underscore',
    'jQuery',
    'text!templates/add_earlier.js',
    'fancybox'
], function (Backbone, _, $, tpl) {
    var self;

    return Backbone.View.extend({
        el:"#toolbar",
        modal:"#inline-earlier",
        text:"#earlier-activity",
        from: "#earlier-calendar-from",
        until: "#earlier-calendar-until",

        events:{
            "click .various":'_openDialog'
        },

        _openDialog: function() {
            $(".various").fancybox({
                fitToView	: true,
                autoSize	: true,
                closeClick	: false,
                openEffect	: 'none',
                closeEffect	: 'none',
                margin: 0,
                padding: 0,
                beforeClose: function() {
                    $(this.from).val("");
                    $(this.until).val("");
                    $(this.text).val("");
                }
            });
        },

        initialize:function () {
            self = this;

            var formHtml = _.template(tpl);

            $(self.modal).html(formHtml);

            $(this.from).datetimepicker({
                "dateFormat": 'dd/mm/yy',
                "maxDate": moment().format("DD/MM/YY")
            });
            $(this.until).datetimepicker({
                "dateFormat": 'dd/mm/yy',
                "maxDate": moment().format("DD/MM/YY")
            });
        },

        render:function () {
            return this;
        }
    });
});