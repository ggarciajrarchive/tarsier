define([
    'backbone',
    'underscore',
    'jQuery',
    'operation',
    'modules/tags_operations',
    'modules/validations',
    'models/state',
    'fancybox',
    'jquerytimpicker'
], function (Backbone, _, $, Operation, TagsOperation, Validation, State) {
    var self;

    return Backbone.View.extend({
        el:"#inline-earlier",
        actInput: "#earlier-activity",
        from: "#earlier-calendar-from",
        until: "#earlier-calendar-until",

        events:{
            "click .close-earlier-modal":'_closeDialog',
            "click .create-earlier-task":'_createActivity',
            "focus from":'_cleanError',
            "focus until":'_cleanError'
        },

        _cleanForm: function() {
            $(self.actInput).val("");
            $(self.from).val("");
            $(self.until).val("");

            var errorBox = self.$el.find(".alert-error");
            $(errorBox).html();
            $(errorBox).hide();
        },

        _closeDialog: function() {
            self._cleanForm();
            $.fancybox.close();
        },

        _cleanError: function() {
            var errorBox = self.$el.find(".alert-error");
            $(errorBox).html();
            $(errorBox).hide();
        },

        _createActivity: function() {
            var errorBox = self.$el.find(".alert-error");

            if (Validation.startLessThanEnd($(self.from).val(), $(self.until).val())) {
                Operation.addWithDate(
                    $("#earlier-activity").val(),
                    $(self.from).val(),
                    $(self.until).val()
                );

                self._cleanForm();

                State.get("days").trigger("change");

                self._cleanError();

                $.fancybox.close();
            } else {
                $(errorBox).html("Start date should be less than the end date.");
                $(errorBox).show();

                return false;
            }
        },

        initialize:function () {
            self = this;

            $(self.actInput)
                .keydown(function(event) {
                    var menuActive = $(this).data('autocomplete').menu.active;
                    if (event.keyCode === $.ui.keyCode.TAB && menuActive)
                        event.preventDefault();
                })
                .autocomplete({
                    source: function(request, response) {
                        var tags =  State.get("tags").pluck("tag");

                        var filtered = _.filter(tags, function(t) {
                            var input = $(self.actInput).val();
                            var searchTerm = input.substring(input.lastIndexOf("#") + 1, input.length);

                            return t.search(searchTerm) >= 0;
                        });

                        response(filtered);
                    },
                    focus: function(event, ui) {
                        return false;
                    },
                    select: function(event, ui) {
                        TagsOperation.appendTag(ui.item.value, self.actInput);
                        $(this).autocomplete('disable');
                        return false;
                    }
                })
                .autocomplete('disable')
                .on('keypress', function(event) {
                    //Detect whether '#' was keyed.
                    if (event.shiftKey
                        && (event.keyCode === 35 || event.charCode === 35)) {
                        $(this).autocomplete('enable');
                        $(self.actInput).val($(self.actInput).val() + "#");
                        return false;
                    }
                });
        },

        render:function () {
            return this;
        }
    });
});