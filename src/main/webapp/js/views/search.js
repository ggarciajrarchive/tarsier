define([
    'backbone',
    'underscore',
    'jQuery',
    'moment',
    'operation',
    'modules/tags_operations',
    'models/state',
    'jqueryui'
], function (Backbone, _, $, moment, Operation, TagsOperation, State) {
    var self;

    return Backbone.View.extend({
        el:"#search-box",
        from:"#calendar-from",
        until:"#calendar-until",
        search:"#search",
        export:"#export",
        textFrom:"#search-box .from",
        textUntil:"#search-box .until",
        textTotal:"#search-box .total",
        textTags:"#search-box #tags",

        reportForm:"#report-form",
        reportFormat:"#report-format",

        events:{
            "click #search":'_search',
            "click #export":'_export'
        },

        initialize:function () {
            self = this;

            $(self.from).datepicker({
                "dateFormat":'dd/mm/yy',
                "maxDate": moment().format("DD/MM/YY")
            });
            $(self.until).datepicker({
                "dateFormat":'dd/mm/yy',
                "maxDate": moment().format("DD/MM/YY")
            });

            $(self.textTags)
                .keydown(function(event) {
                    var menuActive = $(this).data('autocomplete').menu.active;
                    if (event.keyCode === $.ui.keyCode.TAB && menuActive)
                        event.preventDefault();
                })
                .autocomplete({
                    source: function(request, response) {
                        var tags =  State.get("tags").pluck("tag");

                        var filtered = _.filter(tags, function(t) {
                            var input = $(self.textTags).val();
                            var searchTerm = input.substring(input.lastIndexOf("#") + 1, input.length);

                            return t.search(searchTerm) >= 0;
                        });

                        response(filtered);
                    },
                    focus: function(event, ui) {
                        return false;
                    },
                    select: function(event, ui) {
                        TagsOperation.appendTag(ui.item.value, self.textTags);
                        $(this).autocomplete('disable');
                        return false;
                    }
                })
                .autocomplete('disable')
                .on('keypress', function(event) {
                    //Detect whether '#' was keyed.
                    if (event.shiftKey
                        && (event.keyCode === 35 || event.charCode === 35)) {
                        $(this).autocomplete('enable');
                        $(self.textTags).val($(self.textTags).val() + "#");
                        return false;
                    }
                });

            self.updateTotals();
        },

        _search:function () {
            var start = moment($(self.from).val(), "DD/MM/YYYY").format("DD-MM-YYYY");
            var end = moment($(self.until).val(), "DD/MM/YYYY").add('days', 1).format("DD-MM-YYYY");
            var tags = $(self.textTags).val();

            Operation.list(start, end, tags);

            State.get("days").trigger("change");
        },

        _export:function () {
            var from = $(self.from).val();
            var until = $(self.until).val();
            var format = $(self.reportFormat).val();
            var tags = $(self.textTags).val();

            if (from && until) {
                var s = moment(from, "DD/MM/YYYY").format("DD-MM-YYYY");
                var u = moment(until, "DD/MM/YYYY").add('days', 1).format("DD-MM-YYYY");

                var lTags = "";

                if (tags) {
                    _.each(tags.split(" "), function(t) {
                        if (lTags.length > 0)
                            lTags += ",";

                        lTags += t.substr(1);
                    });
                }

                window.open("/report/" + s + "/" + u + "/" + format + "?tags=" + lTags, "_blank");

                return false;
            } else {
                return false;
            }
        },

        updateTotals:function () {
            var from = ($(self.from).val() != "")
                ? $(self.from).val()
                : moment().format("DD/MM/YYYY");

            var until = ($(self.from).val() != "")
                ? $(self.until).val()
                : moment().format("DD/MM/YYYY");

            var days = _.filter(State.get("days").models, function (d) {
                return d.get("day") >= moment(from, "DD/MM/YYYY").format("YYYYMMDD")
                    && d.get("day") <= moment(until, "DD/MM/YYYY").format("YYYYMMDD")
            });

            $(self.textFrom).text(from);
            $(self.textUntil).text(until);
            $(self.textTotal).text(Operation.calculateTotal(days));
        },

        render:function () {
            return this;
        }
    });
});