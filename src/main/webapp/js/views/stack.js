define([
    'backbone',
    'underscore',
    'jQuery',
    'models/state'
], function (Backbone, _, $, State) {
    return Backbone.View.extend({
        el: "#stacks",

        events: {
          "click #manage-tags": "_tagsView",
          "click #manage-tasks": "_tasksView"
        },

        _tagsView: function() {
            this.$el.find("#toolbar #manage-tags").hide();
            this.$el.find("#toolbar #manage-tasks").show();
            this.$el.find("#add-earlier-btn").hide();
            this.$el.find("#st-tags").show();
            this.$el.find("#st-tracker").hide();
            this.$el.find("#st-tags").show();
            return this;
        },

        _tasksView: function() {
            this.$el.find("#toolbar #manage-tags").show();
            this.$el.find("#toolbar #manage-tasks").hide();
            this.$el.find("#add-earlier-btn").show();
            this.$el.find("#st-tracker").show();
            this.$el.find("#st-tags").hide();
            return this;
        },

        initialize: function() {
            this.render();
            return this;
        },

        render: function() {
            this._tasksView();

            return this;
        }
    });
});