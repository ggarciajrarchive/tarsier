define([
    'backbone',
    'underscore',
    'jQuery',
    'operation',
    'modules/tags_operations',
    'models/state'
], function (Backbone, _, $, Operation, TagsOperation, State) {
    var self;

    return Backbone.View.extend({
        el:"#create-task-form",
        actInput:"#activity",
        stop:"#stop",
        createBtn:".create-task",

        events:{
            "click .create-task":'_save',
            "click .stop-task":'_stopRunningTask',
            "keyup #activity":'_toggleAddButton',
        },

        _save:function () {
            Operation.add($("#activity").val());
            TagsOperation.add($("#activity").val());
            $(self.actInput).val("");
            $(self.actInput).blur();
            self._toggleAddButton();
        },

        _stopRunningTask:function () {
            Operation.stop();
        },

        _toggleAddButton:function () {
            if ($(self.actInput).val())
                if ($(self.actInput).val().length > 0) {
                    $(self.createBtn).removeAttr("disabled");
                } else {
                    $(self.createBtn).attr("disabled", "disabled");
                }
        },

        initialize:function () {
            self = this;
            self._toggleAddButton();

            $(self.actInput)
                .keydown(function(event) {
                    var menuActive = $(this).data('autocomplete').menu.active;
                    if (event.keyCode === $.ui.keyCode.TAB && menuActive)
                        event.preventDefault();
                })
                .autocomplete({
                    source: function(request, response) {
                        var tags =  State.get("tags").pluck("tag");

                        var filtered = _.filter(tags, function(t) {
                            var input = $(self.actInput).val();
                            var searchTerm = input.substring(input.lastIndexOf("#") + 1, input.length);

                            return t.search(searchTerm) >= 0;
                        });

                        response(filtered);
                    },
                    focus: function(event, ui) {
                        return false;
                    },
                    select: function(event, ui) {
                        TagsOperation.appendTag(ui.item.value, self.actInput);
                        $(this).autocomplete('disable');
                        return false;
                    }
                })
                .autocomplete('disable')
                .on('keypress', function(event) {
                    //Detect whether '#' was keyed.
                    if (event.shiftKey
                        && (event.keyCode === 35 || event.charCode === 35)) {
                        $(this).autocomplete('enable');
                        $(self.actInput).val($(self.actInput).val() + "#");
                        return false;
                    }
                });
        },

        render:function () {
            return this;
        }
    });
});