define([
    'backbone',
    'underscore',
    'jQuery',
    'moment',
    'operation',
    'models/state',
    'collections/selected_tasks',
    'text!templates/task_template.js',
    'text!templates/edit_delete.js'
], function (Backbone, _, $, moment, Operation, State, SelectedTasksCol, tpl, editDeleteTpl) {
    var self;
    var compiledTpl = _.template(editDeleteTpl);

    return Backbone.View.extend({
        el:"#list-tasks-view",
        list:"#list-tasks",
        toggle:".toggle",
        stop:".stop-task",
        modal:"#inline-edit-delete",

        events:{
            "click .pointer":"_editDelete"
        },

        initialize:function () {
            self = this;
            self.selecteds = new SelectedTasksCol();
            self.template = _.template(tpl);

            var start = moment().format("DD-MM-YYYY");
            var end = moment().add('days', 1).format("DD-MM-YYYY");

            Operation.list(start, end);

            this.render();
        },

        _editDelete:function (eve) {
            var day = $(eve.currentTarget).data("day");

            var task = Operation.find(
                day,
                $(eve.currentTarget).data("id")
            );

            $(self.modal).html(compiledTpl({t:task, day:day}));

            if (task.get("end") == "") {
                $(".reopen-task").attr("disabled","disabled");
            }

            $("#edit-calendar-from").datetimepicker({
                "dateFormat": 'dd/mm/yy',
                "timeFormat": 'hh:mm:ss tt'
            });

            $("#edit-calendar-until").datetimepicker({
                "dateFormat": 'dd/mm/yy',
                "timeFormat": 'hh:mm:ss tt'
            });

            $(".various-edit-delete").fancybox({
                fitToView:true,
                autoSize:true,
                closeClick:false,
                openEffect:'none',
                closeEffect:'none',
                margin:0,
                padding:0
            });

            $(".various-edit-delete").click();
        },

        _toggleStop:function () {
            if (Operation.running()) {
                $(self.stop).removeAttr("disabled");
            } else {
                $(self.stop).attr("disabled", "disabled");
            }
        },

        _refreshTotals:function () {
            _.each(State.get("days").models, function (t) {
                t.set({"totals":Operation.calculateTotalByDay(t.get("tasks").models)});
            });
        },

        _closeDialog: function() {
            $.fancybox.close();
        },

        render:function () {
            self._refreshTotals();

            var html = self.template({'days':State.get("days").models});
            $("#list-tasks").html(html);
            self._toggleStop();

            return this;
        }
    });
});