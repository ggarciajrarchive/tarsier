define([
    'backbone',
    'underscore',
    'jQuery',
    'operation',
    'modules/validations',
    'fancybox',
    'jquerytimpicker'
], function (Backbone, _, $, Operation, Validations) {
    var self;

    return Backbone.View.extend({
        el:"#inline-edit-delete",
        actInput: "#edit-activity",
        from: "#edit-calendar-from",
        until: "#edit-calendar-until",

        events:{
            "click .close-modal":'_closeDialog',
            "click .reopen-task":'_reopenTasks',
            "click .edit-task":'_editActivity',
            "click .delete-task":'_deleteActivity'
        },

        _cleanForm: function() {
            $(self.actInput).val("");
            $(self.from).val("");
            $(self.until).val("");
        },

        _closeDialog: function() {
            self._cleanForm();
            $.fancybox.close();
        },

        _reopenTasks:function (eve) {
            var task = Operation.find(
                $("#day").val(),
                $("#tid").val()
            );

            var content = task.get("task") + task.tagsToString();

            Operation.add(content);

            self._closeDialog();
        },

        _editActivity: function() {
            var errorBox = self.$el.find(".alert-error");

            if (Validations.startLessThanEnd($(self.from).val(), $(self.until).val())) {
                Operation.editWithDate(
                    $("#day").val(),
                    $("#tid").val(),
                    $("#edit-activity").val(),
                    $(self.from).val(),
                    $(self.until).val()
                );

                self._closeDialog();
            } else {
                $(errorBox).html("Start date should be less than the end date.");
                $(errorBox).show();

                return false;
            }
        },

        _deleteActivity: function() {
            Operation.deleteTask(
                $("#day").val(),
                $("#tid").val())
            ;

            self._closeDialog();
        },

        initialize:function () {
            self = this;
        },

        render:function () {
            return this;
        }
    });
});