define([
    'backbone',
    'underscore',
    'moment',
    'models/state',
    'operation',
    'tinyscroll',
    'text!templates/last_days_template.js'
], function (Backbone, _, moment, State, Operation, Tinyscrollbar, taskTpl) {
    var self;
    var compiledTemplate = _.template(taskTpl);
    var from = moment().subtract('days', 5);
    var until = moment();

    return Backbone.View.extend({
        el:"#last-days-box",
        textFrom:"#last-days-box .from",
        textUntil:"#last-days-box .until",
        textTotal:"#last-days-box .total",

        initialize:function () {
            self = this;

            Operation.lastDaysList();

            self.updateTotals();
        },

        _refreshTasks: function(days) {
            self.$el.find(".overview").html(compiledTemplate({'days':days.reverse()}));
            $('#last-days-act-scroll').tinyscrollbar({sizethumb: 15});
            $('#last-days-act-scroll').tinyscrollbar_update();
        },

        updateTotals:function () {
            var days = _.filter(State.get("lastDays").models, function (d) {
                return d.get("day") >= from.format("YYYYMMDD")
                    && d.get("day") <= until.format("YYYYMMDD")
            });

            var total = Operation.calculateTotal(days);

            $(self.textFrom).text(from.format("DD/MM/YYYY"));
            $(self.textUntil).text(until.format("DD/MM/YYYY"));
            $(self.textTotal).text(total);

            if (total != "00:00:00") {
                self.$el.show();
                self._refreshTasks(days);
            } else {
                self.$el.hide();
            }
        },

        render:function () {
            return this;
        }
    });
});