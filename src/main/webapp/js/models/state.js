define([
    'backbone'
], function(Backbone) {
    var State = Backbone.Model.extend({});
    return new State();
});