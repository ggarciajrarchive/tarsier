define([
    'backbone'
], function(Backbone) {
    return Backbone.Model.extend({
        urlRoot: '/api/tasks.json',
        tagsToString:function() {
            var str = "";

            _.each(this.get("tags"), function(t) {
                var prefix = (t.tag.indexOf("#") >= 0) ? "" : "#";
                str += " " + prefix + t.tag;
            });

            return str;
        }
    });
});