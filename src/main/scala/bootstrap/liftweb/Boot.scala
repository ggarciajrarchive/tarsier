package bootstrap.liftweb

import net.liftweb._

import common.{Empty, Full}
import http._
import http.Html5Properties
import mapper.{Schemifier, DB}
import sitemap._
import Loc._
import sitemap.Loc.If
import util.Helpers._
import inf.eureka.tarsier.lib.AppSetup
import inf.eureka.tarsier.model._
import inf.eureka.tarsier.rest._

/**
 * A class that's instantiated early and run.  It allows the application
 * to modify lift's environment
 */
class Boot {
  def boot() {
    // where to search snippet
    LiftRules.addToPackages("inf.eureka.tarsier")

    // Configure mailer
    AppSetup.configMailer()

    // Configure db data source
    AppSetup.configDbDataSource()

    // Use Lift's Mapper ORM to populate the database
    // you don't need to use Mapper to use Lift... use
    // any ORM you want
    Schemifier.schemify(true, Schemifier.infoF _,
      User,
      Tag,
      Task,
      TaskTags)

    val AuthRequired = If(() => User.loggedIn_?,
      () => RedirectResponse("/"))

    // Build SiteMap
    val entries =
      Menu(Loc("Home", "index" :: Nil, "Home")) ::
      Menu(Loc("Tracker", "tracker" :: Nil, "Tracker", Hidden, AuthRequired)) ::
      Menu(Loc("Manual", "manual" :: Nil, "Manual")) ::
      Menu(Loc("403", "403" :: Nil, "403", Hidden)) ::
      Menu(Loc("404", "404" :: Nil, "404", Hidden)) ::
      Menu(Loc("500", "500" :: Nil, "500", Hidden)) ::
      User.menus

    // set the sitemap.  Note if you don't want access control for
    // each page, just comment this line out.
    LiftRules.setSiteMap(SiteMap(entries: _*))

    // prevent lift from serving static files
    LiftRules.liftRequest.append {
      case Req("classpath" :: _, _, _) => true
      case Req("ajax_request" :: _, _, _) => true
      case Req("user-images" :: _ :: Nil, _, GetRequest) => true
      case Req("favicon" :: Nil, "ico", GetRequest) => false
      case Req(_, "css", GetRequest) => false
      case Req(_, "js", GetRequest) => false
      case Req(_, "images", GetRequest) => false
    }

    // Tasks Rest Service
    LiftRules.dispatch.append(TagsRest)
    LiftRules.dispatch.append(TasksRest)
    LiftRules.dispatch.append(ReportRest)

    // Reponse redirects so we can use custom error templates
    LiftRules.responseTransformers.append {
      case r if r.toResponse.code == 403 => RedirectResponse("/403")
      case r if r.toResponse.code == 404 => RedirectResponse("/404")
      case r if r.toResponse.code == 500 => RedirectResponse("/500")
      case r => r
    }

    // What is the function to test if a user is logged in?
    LiftRules.loggedInTest = Full(() => User.loggedIn_?)

    // Logout
    LiftRules.dispatch.append {
      case Req("logout" :: Nil, _, GetRequest) =>
        S.request.foreach(_.request.session.terminate) //
        S.redirectTo("/")
    }

    // Fade out messages
    LiftRules.noticesAutoFadeOut.default.set((notices: NoticeType.Value) => {
      notices match {
        case NoticeType.Notice => Full((1 seconds, 2 seconds))
        case NoticeType.Warning => Full((1 seconds, 2 seconds))
        case NoticeType.Error => Full((1 seconds, 3 seconds))
        case _ => Empty
      }
    }
    )

    // Show the spinny image when an Ajax call starts
    LiftRules.ajaxStart =
      Full(() => LiftRules.jsArtifacts.show("ajax-loader").cmd)

    // Make the spinny image go away when it ends
    LiftRules.ajaxEnd =
      Full(() => LiftRules.jsArtifacts.hide("ajax-loader").cmd)

    // Force the request to be UTF-8
    LiftRules.early.append(_.setCharacterEncoding("UTF-8"))

    // HTML 5
    LiftRules.htmlProperties.default.set((r: Req) => new Html5Properties(r.userAgent))

    // Make a transaction span the whole HTTP request
    S.addAround(DB.buildLoanWrapper())
  }
}
