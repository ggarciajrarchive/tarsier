package inf.eureka.tarsier.model

import net.liftweb.mapper._

/**
 * User: colt44
 * Date: 8/12/12
 * Time: 6:55 PM
 */
object Tag extends Tag with LongKeyedMetaMapper[Tag] {
  override def dbTableName = "tags"
}

class Tag extends LongKeyedMapper[Tag] with ManyToMany {
  def getSingleton = Tag

  def primaryKeyField = id

  object id extends MappedLongIndex(this)

  object tag extends MappedString(this, 10)

  object tasks extends MappedManyToMany(TaskTags, TaskTags.tag, TaskTags.task, Task)

  object user extends MappedLongForeignKey(this, User)
}


object TaskTags extends TaskTags with LongKeyedMetaMapper[TaskTags] {
  def deleteByTask(task: Task) {
    TaskTags.bulkDelete_!!(By(TaskTags.task, task))
  }

  override def dbTableName = "tasktags"
}

class TaskTags extends LongKeyedMapper[TaskTags] with IdPK {
  def getSingleton = TaskTags

  object task extends MappedLongForeignKey(this, Task)

  object tag extends MappedLongForeignKey(this, Tag)

}
