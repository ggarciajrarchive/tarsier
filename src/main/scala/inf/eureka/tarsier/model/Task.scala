package inf.eureka.tarsier.model

import net.liftweb.mapper._
import net.liftweb.common.Box
import java.util.{Date => UDate}
import java.sql.{Date => SDate}

/**
 * Task: colt44
 * Date: 8/12/12
 * Time: 6:52 PM
 */

object Task extends Task with LongKeyedMetaMapper[Task] {
  override def dbTableName = "tasks"

  def getRunning: Box[Task] = {
    Task.findAllByPreparedStatement({
      superconn =>
        val query =
          """
          select id
            from tasks
           where user_c = ?
             and end_c is null
          """

        val ps = superconn.connection.prepareStatement(query)
        ps.setLong(1, User.currentUserId.openTheBox.toLong)
        ps
    }).headOption
  }

  def listByPeriod(start: UDate, end: UDate) = {
    implicit def uDt2sDt(uDt: UDate) = new SDate(uDt.getTime)

    Task.findAllByPreparedStatement({
      superconn =>
        val query =
          """
            select *
              from tasks
             where user_c = ?
               and start_c between ? and ?
               and (end_c <= ? or end_c is null)
             order by start_c asc
          """

        val ps = superconn.connection.prepareStatement(query)
        ps.setLong(1, User.currentUserId.openTheBox.toLong)
        ps.setDate(2, start)
        ps.setDate(3, end)
        ps.setDate(4, end)
        ps
    })
  }

  def listByPeriodAndTags(start: UDate, end: UDate, tags: String) = {
    implicit def uDt2sDt(uDt: UDate) = new SDate(uDt.getTime)

    Task.findAllByPreparedStatement({
      superconn =>
        val query =
          """
            select t.*
              from tasks t
              join tasktags tt
                on t.id = tt.task_c
              join tags tg
                on tt.tag = tg.id
             where t.user_c = ?
               and tg.tag in (""" + tags + """)
               and t.start_c between ? and ?
               and (t.end_c <= ? or t.end_c is null)
             order by t.start_c asc
          """

        val ps = superconn.connection.prepareStatement(query)
        ps.setLong(1, User.currentUserId.openTheBox.toLong)
        ps.setDate(2, start)
        ps.setDate(3, end)
        ps.setDate(4, end)
        ps
    })
  }
}

class Task extends LongKeyedMapper[Task] with OneToMany[Long, Task] with ManyToMany {
  def getSingleton = Task

  def primaryKeyField = id

  object id extends MappedLongIndex(this)

  object content extends MappedString(this, 140)

  object tags extends MappedManyToMany(TaskTags, TaskTags.task, TaskTags.tag, Tag)

  object user extends MappedLongForeignKey(this, User)

  object start extends MappedDateTime(this)

  object end extends MappedDateTime(this)

}
