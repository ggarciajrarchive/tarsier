package inf.eureka.tarsier.model

import net.liftweb.mapper._
import net.liftweb.common.{Box, Full}
import net.liftweb.sitemap.{Loc, Menu}
import net.liftweb.http.{SHtml, Templates, S}
import net.liftweb.util.BindHelpers._
import net.liftweb.http.S.LFuncHolder

/**
 * User: colt44
 * Date: 8/12/12
 * Time: 4:14 PM
 */
object User extends User
with MetaMegaProtoUser[User] {
  /* overriding paths */
  override def basePath = "user" :: Nil
  override def validateUserSuffix = "validate"
  override def passwordResetSuffix = "reset-password"

  /* overriding user menu to get friendly url*/
  override def loginMenuLoc: Box[Menu] = Full(Menu(Loc("Login", "login" :: Nil, "Login")))
  override def logoutMenuLoc: Box[Menu] = Full(Menu(Loc("Logout", "logout" :: Nil, "Logout")))
  override def createUserMenuLoc: Box[Menu] = Full(Menu(Loc("Sign Up", "signup" :: Nil, "Sign Up")))
  override def editUserMenuLoc: Box[Menu] = Full(Menu(Loc("Edit Profile", "profile" :: Nil, "Edit Profile")))

  override def lostPasswordMenuLoc: Box[Menu] =
    Full(Menu(Loc("Lost Password", "password" :: "lost" :: Nil, "Lost Password")))
  override def changePasswordMenuLoc: Box[Menu] =
    Full(Menu(Loc("Change Password", "password" :: "change" :: Nil, "Change Password")))

  /* overriding login template */
  override def login = {
    def logInUser() {
      S.param("email").flatMap(email => findUserByUserName(email)) match {
        case Full(user) if user.validated_? &&
          user.testPassword(S.param("password")) => {
          logUserIn(user, () => {
            S.redirectTo("/tracker")
          })
        }
        case Full(user) if !user.validated_? =>
          S.error(S.??("account.validation.error"))
        case _ => S.error(S.??("invalid.credentials"))
      }
    }

    val loginXthml = Templates("templates-hidden" :: "account" :: "login" :: Nil).
      openOr(<span>Template not found</span>)

    ("#email [value]" #> S.param("email").openOr("") &
      "button" #> SHtml.submit(
        S.??("log.in"),
        logInUser,
        "class" -> "btn btn-primary"
      ))(loginXthml)
  }

  /* overriding lost password template */
  override def lostPassword = {
    val lostPwdXthml =
      Templates("templates-hidden" :: "account" :: "lost-password" :: Nil).
        openOr(<span>Template not found</span>)

    ("#email" #>
      SHtml.text("",
        sendPasswordReset _,
        "class" -> "input-text") &
      ".button" #> SHtml.submit(
        S.??("send.it"),
        () => {},
        "class" -> "nice small radius blue button"))(lostPwdXthml)
  }

  /* overriding reset password template */
  override def passwordReset(id: String) = {
    val resetPwdXthml =
      Templates("templates-hidden" :: "account" :: "reset-password" :: Nil).
        openOr(<span>Template not found</span>)

    findUserByUniqueId(id) match {
      case Full(user) =>
        def reset() {
          user.validate match {
            case Nil =>
              user.save()
              user.resetUniqueId().save()

              logUserIn(user, () => {
                S.redirectTo(homePage, () => S.notice(S.??("password.changed")))
              })

            case xs => {
              user.resetUniqueId().save()
              S.redirectTo(
                "/user/reset-password/" + user.uniqueId.is,
                () => S.error(S.??("password.reset.problem"))
              )
            }
          }
        }

        ("input" #> SHtml.password_*(
          "",
          (p: List[String]) => user.setPasswordFromListString(p)
        ) &
          "button" #> SHtml.submit(S.??("set.password"),
            reset _, "class" -> "btn btn-primary"))(resetPwdXthml)
      case _ => S.error(S.??("password.link.invalid")); S.redirectTo(homePage)
    }
  }

  /* overriding change password template */
  override def changePassword = {
    val user = currentUser.open_!
    var oldPwd = ""
    var newPwd: List[String] = Nil

    def changePass() {
      if (user.testPassword(Full(oldPwd))) {
        user.setPasswordFromListString(newPwd)
        user.validate match {
          case Nil => {
            user.save()
            S.notice(S.??("password.changed"))
          }
          case xs => S.error(xs.flatMap(e => {
            <li>{e.msg}</li>
          }))
        }
      } else {
        S.error(S.??("wrong.old.password"))
      }
    }

    val changeXhtml =
      Templates("templates-hidden" :: "account" :: "change-password" :: Nil).
        openOr(<span>Template not found</span>)

    ("#old" #> SHtml.password("", pwd => oldPwd = pwd, "class" -> "input-xlarge") &
      ".new" #> SHtml.password_*(
        "",
        LFuncHolder(s => newPwd = s),
        "class" -> "input-xlarge") &
      "button" #> SHtml.submit(
        S.??("change"),
        changePass,
        "class" -> "btn btn-primary"
      ))(changeXhtml)
  }

  /* overriding edit profile */
  override def edit = {
    val user = currentUser.open_!

    val editTemplate =
      Templates("templates-hidden" :: "account" :: "edit" :: Nil).
        openOr(<span>Template not found</span>)

    def save() {
      user.save()

      logUserIn(user, () => {
        S.redirectTo(homePage, () => S.notice(S.??("account.edited")))
      })
    }

    ("h2 *" #> S.??("edit") &
      ".fields" #> localForm(user, false, editFields) &
      ".button" #> SHtml.button(
        S.??("save"),
        save _,
        "class" -> "nice small radius blue button")
      )(editTemplate)
  }

  override def dbTableName = "users"

  override def signupFields = List(firstName, lastName, email, password)

  override def editFields = List(firstName, lastName, email, password)

  override def emailFrom = "no-reply@eureka.inf.br"
}

class User extends MegaProtoUser[User] {
  def getSingleton = User

  def meta = User
}
