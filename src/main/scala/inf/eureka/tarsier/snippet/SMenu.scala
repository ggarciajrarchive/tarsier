package inf.eureka.tarsier.snippet

import net.liftweb.util.BindHelpers._
import xml.{Text, NodeSeq}
import net.liftweb.http.{SHtml, S, Templates}
import inf.eureka.tarsier.model.User

/**
 * User: colt44
 * Date: 8/12/12
 * Time: 4:00 PM
 */

class SMenu {
  def left = {
    val tpl = Templates("templates-hidden" :: "navbar" :: "left" :: Nil).
      openOr(<span>No template found</span>)

    val home = ".mn-home [class]" #> (if (S.uri.equals("/")) "mn-home active" else "mn-home")

    val tracker =
      if (User.loggedIn_?)
        ".mn-tracker *" #> SHtml.link("/tracker", () => {}, Text("Tracker"))
      else
        ".mn-tracker *" #> NodeSeq.Empty

    val trackerActivate = ".mn-tracker [class]" #>
      (if (S.uri.equals("/tracker")) "mn-tracker active" else "mn-tracker")

    val manual = ".mn-manual [class]" #> (if (S.uri.equals("/manual")) "mn-manual active" else "mn-manual")

    (home & tracker & manual & trackerActivate)(tpl)
  }

  def right = {
    val tpl = Templates("templates-hidden" :: "navbar" :: "right" :: Nil).
      openOr(<span>No template found</span>)

    val signup =
      if (User.loggedIn_?)
        ".mn-signup *" #> NodeSeq.Empty
      else
        ".mn-signup *" #> SHtml.link("/signup", () => {}, Text("Sign Up"))

    val signupActivate = ".mn-signup [class]" #>
      (if (S.uri.equals("/signup")) "mn-signup active" else "mn-signup")

    val divider =
      if (User.loggedIn_?)
        ".mn-divider *" #> NodeSeq.Empty
      else
        ".mn-divider *" #> "|"

    val login =
      if (User.loggedIn_?)
      ".mn-login *" #> NodeSeq.Empty
      else
        ".mn-login *" #> SHtml.link("/login", () => {}, Text("Log In"))

    val loginActivate = ".mn-login [class]" #>
      (if (S.uri.equals("/login")) "mn-login active" else "mn-login")

    val user =
      if (User.loggedIn_?)
      ".user-name" #> User.currentUser.openTheBox.shortName
      else
        ".mn-user *" #> NodeSeq.Empty

    val userActivate = ".mn-user [class]" #>
      (if (S.uri.equals("/profile") || S.uri.equals("/password/change")) "mn-user active" else "mn-user")

    (signup & signupActivate & divider & login & loginActivate & user & userActivate)(tpl)
  }
}
