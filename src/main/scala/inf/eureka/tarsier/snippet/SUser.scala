package inf.eureka.tarsier.snippet

import net.liftweb.http.{SHtml, S, RequestVar}
import net.liftweb.util.BindHelpers._
import inf.eureka.tarsier.lib.Validator
import inf.eureka.tarsier.model.User

/**
 * User: colt44
 * Date: 9/8/12
 * Time: 6:51 PM
 */
class SUser {
  def login = User.login

  def lostpassword = User.lostPassword

  def changepassword = User.changePassword

  def signup = {
    object fname extends RequestVar("")
    object lname extends RequestVar("")
    object email extends RequestVar("")
    object pwd extends RequestVar("")
    object confirmPwd extends RequestVar("")

    def save() {
      val user = User.create.firstName(fname).lastName(lname).email(email).password(pwd)

      val errors =
        Validator.validateMinLength(fname, 1, "First name is mandatory!") ++
          Validator.validateMinLength(lname, 1, "Last name is mandatory!") ++
          Validator.validateMinLength(pwd, 6, "Password should have at least 6 chars!") ++
          Validator.validateEmail(email, "E-mail is not valid!") ++
          Validator.validateMatches(pwd, confirmPwd, "Password and Confirmation doesn't match!")

      errors match {
        case Nil => {
          user.save()

          User.sendValidationEmail(user)

          S.redirectTo("/", () => S.notice(S.??("account.created")))
        }
        case ers => {
          S.error(ers.filter((e) => e.length > 0).flatMap(e => {<li>{e}</li>}))
        }
      }
    }

    "#fname" #> SHtml.text(fname, fname(_), "id" -> "name", "class" -> "input-xlarge") &
      "#lname" #> SHtml.text(lname, lname(_), "id" -> "name", "class" -> "input-xlarge") &
      "#email" #> SHtml.text(email, email(_), "id" -> "email", "class" -> "input-xlarge") &
      "#pwd" #> SHtml.password(pwd, pwd(_), "id" -> "pwd", "class" -> "input-xlarge") &
      "#confirm_pwd" #> SHtml.password(confirmPwd, confirmPwd(_), "id" -> "confirm_pwd", "class" -> "input-xlarge") &
      ".btn-primary" #> SHtml.submit("Sign", () => save(), "class" -> "btn btn-primary")
  }

  def edit = {
    val user = User.currentUser.get

    object fname extends RequestVar(user.firstName.is)
    object lname extends RequestVar(user.lastName.is)
    object email extends RequestVar(user.email.is)
    object pwd extends RequestVar("")
    object confirmPwd extends RequestVar("")

    def save() {
      user.firstName(fname).lastName(lname).email(email).password(pwd)

      val errors =
        Validator.validateMinLength(fname, 1, "First name is mandatory!") ++
          Validator.validateMinLength(lname, 1, "Last name is mandatory!") ++
          Validator.validateMinLength(pwd, 6, "Password should have at least 6 chars!") ++
          Validator.validateEmail(email, "E-mail is not valid!") ++
          Validator.validateMatches(pwd, confirmPwd, "Password and Confirmation doesn't match!")

      errors match {
        case Nil => {
          user.save()

          S.redirectTo("/tracker", () => S.notice(S.??("Profile successfully updated!")))
        }
        case ers => {
          S.error(ers.filter((e) => e.length > 0).flatMap(e => {<li>{e}</li>}))
        }
      }
    }

    "#fname" #> SHtml.text(fname, fname(_), "id" -> "name", "class" -> "input-xlarge") &
      "#lname" #> SHtml.text(lname, lname(_), "id" -> "name", "class" -> "input-xlarge") &
      "#email" #> SHtml.text(email, email(_), "id" -> "email", "class" -> "input-xlarge") &
      "#pwd" #> SHtml.password(pwd, pwd(_), "id" -> "pwd", "class" -> "input-xlarge") &
      "#confirm_pwd" #> SHtml.password(confirmPwd, confirmPwd(_), "id" -> "confirm_pwd", "class" -> "input-xlarge") &
      ".btn-primary" #> SHtml.submit("Save", () => save(), "class" -> "btn btn-primary")
  }
}
