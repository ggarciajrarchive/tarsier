package inf.eureka.tarsier.session

import net.liftweb.http.SessionVar
import inf.eureka.tarsier.model.User
import net.liftweb.common.{Empty, Box}

/**
 * User: colt44
 * Date: 8/12/12
 * Time: 4:13 PM
 */

object userSess extends SessionVar[Box[User]](Empty)
