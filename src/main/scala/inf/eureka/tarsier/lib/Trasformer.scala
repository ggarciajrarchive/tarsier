package inf.eureka.tarsier.lib

import inf.eureka.tarsier.model.Task
import collection.immutable.HashMap
import inf.eureka.tarsier.lib.Formatter._

/**
 * User: colt44
 * Date: 9/2/12
 * Time: 11:30 AM
 */
object Trasformer {
  def list2map(tasks: List[Task]): HashMap[Long, List[Task]] = {
    var m = new HashMap[Long, List[Task]]

    tasks.map(t => {
      val k = format(t.start, "yyyyMMdd").toLong

      m.get(k) match {
        case Some(f) => m += (k -> (t :: f))
        case _ => m += (k -> (t :: Nil))
      }

      m
    })

    m
  }
}
