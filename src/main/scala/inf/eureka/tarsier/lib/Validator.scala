package inf.eureka.tarsier.lib

import org.apache.commons.validator.routines.EmailValidator

/**
 * User: colt44
 * Date: 8/26/12
 * Time: 1:37 AM
 */
object Validator {
  def validateMinLength(value: String, minLength: Int, msg: String) = {
    if (value.length < minLength) {
      List(msg)
    } else {
      Nil
    }
  }

  def validateEmail(str: String, msg: String) = {
    println(EmailValidator.getInstance().isValid(str))
    if (EmailValidator.getInstance().isValid(str)) Nil else List(msg)
  }

  def validateMatches(str: String, otherStr: String, msg: String) = {
    if (str.equals(otherStr)) Nil else List(msg)
  }
}
