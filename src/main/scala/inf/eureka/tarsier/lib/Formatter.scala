package inf.eureka.tarsier.lib

import java.text.SimpleDateFormat
import java.util.Date
import net.liftweb.common.{Empty, Full, Box}
import net.liftweb.mapper.MappedDateTime
import inf.eureka.tarsier.model.Task

/**
 * User: colt44
 * Date: 8/12/12
 * Time: 9:50 PM
 */

object Formatter {
  implicit def mpdDate2BoxDate(dt: MappedDateTime[Task]) = {
    if (dt != null && dt.get != null) Full(dt.get) else Empty
  }

  def format(dt: Box[Date], fmt: String) = dt match {
    case Full(d) => (new SimpleDateFormat(fmt)).format(d)
    case _ => ""
  }

  def parse(dt: Box[String], fmt: String) = dt match {
    case Full(d) => (new SimpleDateFormat(fmt)).parse(d)
    case _ => new Date
  }

  def format(l: Long) = {
    val hours: Int = (l / 3600).toInt
    val remainder: Int = (l % 3600).toInt
    val minutes: Int = remainder / 60
    val seconds: Int = remainder % 60

    val ftm = (if (hours < 10) "0" else "") + hours + ":" +
      (if (minutes < 10) "0" else "") + minutes + ":" +
      (if (seconds < 10) "0" else "") + seconds

    ftm
  }
}
