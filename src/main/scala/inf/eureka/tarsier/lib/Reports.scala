package inf.eureka.tarsier.lib

import net.liftweb.http._
import net.liftweb.util.BindHelpers._
import inf.eureka.tarsier.model.Task
import net.liftweb.common.{Box, Full}
import inf.eureka.tarsier.lib.Formatter._
import inf.eureka.tarsier.lib.Trasformer._
import java.util.Date
import collection.immutable.HashMap

/**
 * User: colt44
 * Date: 9/2/12
 * Time: 11:22 AM
 */
object Reports {
  private def getTotal(l: List[Task]) = {
    def seconds(s: Date, e: Date) = {
      import org.joda.time.{Period, Interval, DateTime}

      val t1Period: Period = new Interval(
        new DateTime(s),
        new DateTime(if (e == null) new Date else e)
      ).toPeriod

      t1Period.getSeconds + (t1Period.getMinutes * 60) + (t1Period.getHours * 60 * 60)
    }

    l.foldLeft(0L)((sec, t1: Task) => sec + seconds(t1.start, t1.end))
  }

  private def fetch(from: String, until: String, tags: Box[String]) = {
    def listByPeriod = Task.listByPeriod(
      parse(Full(from + " 00:00:00"), "dd-MM-yyyy HH:mm:ss"),
      parse(Full(until + " 00:00:00"), "dd-MM-yyyy HH:mm:ss")
    )

    tags match {
      case Full(p) if p.length > 0 => {
        Task.listByPeriodAndTags(
          parse(Full(from + " 00:00:00"), "dd-MM-yyyy HH:mm:ss"),
          parse(Full(until + " 00:00:00"), "dd-MM-yyyy HH:mm:ss"),
          p.split(",").map(t => "'" + t + "'").mkString(",")
        )
      }
      case Full(t) => listByPeriod
      case _ => listByPeriod
    }
  }

  def html(from: String, until: String, tags: Box[String]) = {
    val mainTpl = Templates("templates-hidden" :: "reports" :: Nil).
      openOr(<span>Template not found</span>)

    val tpl = Templates("templates-hidden" :: "reports" :: "html-report" :: Nil).
      openOr(<span>Template not found</span>)

    val sDate = parse(Full(from), "dd-MM-yyyy")
    val eDate = parse(Full(until), "dd-MM-yyyy")

    val tasks = fetch(from, until, tags)

    val map = list2map(fetch(from, until, tags))

    val parsedTaks = (".list" #> list(map) &
      ".summary" #> summary(sDate, eDate, getTotal(tasks)))(tpl)

    ("#content" #> parsedTaks)(mainTpl)
  }

  def csv(from: String, until: String, tags: Box[String]) = {
    val sDate = parse(Full(from), "dd-MM-yyyy")
    val eDate = parse(Full(until), "dd-MM-yyyy")

    val tasks = fetch(from, until, tags) sortWith(
      (t1, t2) =>
        format(t1.start, "yyyyMMddHHmmss").toLong < format(t2.start, "yyyyMMddHHmmss").toLong
      )

    val parsedTasks = tasks.map(t => {
      t.content + "," +
      t.tags.map(t => "#" + t.tag.get).mkString(" ") + "," +
      format(t.start, "dd/MM/yyyy HH:mm") + "," +
      format(t.end, "dd/MM/yyyy HH:mm")
    }).mkString("\n")

    parsedTasks + "\n" +
    "Total of Worked hours from " + format(Full(sDate), "dd/MM/yyyy") + " until " +
      format(Full(eDate), "dd/MM/yyyy") + " is " + format(getTotal(tasks))
  }

  def list(map: HashMap[Long, scala.List[Task]]) = {
    val tpl = Templates("templates-hidden" :: "reports" :: "html-report-tasks" :: Nil).
      openOr(<span>No template found</span>)

    "*" #> map.keys.toList.sortWith((k1, k2) => k1 < k2).map(k => {
      val tasks = map.get(k).get.sortWith(
        (t1, t2) =>
          format(t1.start, "yyyyMMddHHmmss").toLong < format(t2.start, "yyyyMMddHHmmss").toLong
      )
      (".tday-date *" #> format(tasks.head.start, "E, MMM d, yyyy") &
        ".tday-total *" #> format(getTotal(tasks)) &
        ".trows *" #> (tasks.map(t => {
          ".check" #> SHtml.checkbox(false, (b) => {}) &
            ".tcontent" #> t.content &
            ".ttags *" #> (
              if (t.tags.size > 0) {
                t.tags.map(t => "#" + t.tag.get).mkString(",")
              } else ""
              ) &
            ".tstart *" #> format(t.start, "HH:mm") &
            ".tend *" #> format(t.end, "HH:mm")
        })))(tpl)
    })
  }

  def summary(from: Date, until: Date, totals: Long) = {
    ".from *" #> format(Full(from), "dd/MM/yyyy") &
      ".until *" #> format(Full(until), "dd/MM/yyyy") &
      ".total *" #> format(totals)
  }
}
