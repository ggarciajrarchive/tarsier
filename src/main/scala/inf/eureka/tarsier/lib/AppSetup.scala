package inf.eureka.tarsier.lib

import net.liftweb.common.{Full, Loggable}
import net.liftweb.mapper._
import net.liftweb.util.Mailer
import javax.mail.internet.{MimeMultipart, InternetAddress, MimeMessage}
import javax.mail.Message.RecipientType._
import net.liftweb.common.Full

/**
 * User: colt44
 * Date: 8/12/12
 * Time: 7:21 PM
 */

object AppSetup extends Loggable {
  def configDbDataSource() {
    logger.info("Configuring database data source")

    DefaultConnectionIdentifier.jndiName = "jdbc/dstarsier"
  }

  def configMailer() {
    logger.info("Configuring Mailer")

    Mailer.jndiName = Full("mail/Session")

    Mailer.testModeSend.default.set((m: MimeMessage) => {
      val from = m.getFrom.map(f => {
        val x = f.asInstanceOf[InternetAddress]
        x.getAddress
      }).mkString(",")

      val to = m.getRecipients(TO).map(f => {
        val x = f.asInstanceOf[InternetAddress]
        x.getAddress
      }).mkString(",")

      val content = m match {
        case mm: MimeMultipart => mm.getBodyPart(1).getContent
        case otherwise => {
          val x = otherwise.getContent.asInstanceOf[MimeMultipart]
          x.getBodyPart(0).getContent
        }
      }

      val log =
        """
          Sending email...
          From: """ + from + """
          To: """ + to + """
          Subject: """ + m.getSubject + """
          Content: """ + content + """
                                   """.stripMargin
      logger.info("Sending email...\n" + log)
    })
  }
}
