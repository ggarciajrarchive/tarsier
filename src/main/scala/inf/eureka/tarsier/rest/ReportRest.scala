package inf.eureka.tarsier.rest

import net.liftweb.http._
import net.liftweb.http.rest.RestHelper
import net.liftweb.common.{Full, Box}

import inf.eureka.tarsier.lib.Reports._
import xml.NodeSeq

/**
 * User: colt44
 * Date: 9/2/12
 * Time: 10:34 AM
 */
object ReportRest extends RestHelper {
  serve {
    case Req("report" :: from :: until :: format :: _, "", GetRequest) => report(from, until, format)
  }

  def report(from: String, until: String, format: String): () => Box[LiftResponse] = {
    format match {
      case "csv" => string2StreamResponse(csv(from, until, S.param("tags")), "text/csv", from, until, format)
      case "html" => nodeSeq2StreamResponse(html(from, until, S.param("tags")), from, until, format)
      case _ => ForbiddenResponse("Invalid report option")
    }
  }

  private def string2StreamResponse(str: String,
                                    contentType: String,
                                    from: String,
                                    until: String,
                                    format: String) = {
    val data: Array[Byte] = str.getBytes

    val contentDisposition = "attachment; filname=tarsier-report-" + from + "-" + until + "." + format

    val headers =
      ("Content-type" -> contentType) ::
        ("Content-length" -> data.length.toString) ::
        ("Content-disposition" -> contentDisposition) :: Nil

    Full(StreamingResponse(
      new java.io.ByteArrayInputStream(data),
      () => {},
      data.length,
      headers, Nil, 200)
    )
  }

  private def nodeSeq2StreamResponse(nodeSeq: NodeSeq,
                                     from: String,
                                     until: String,
                                     format: String) =
    string2StreamResponse(nodeSeq.toString(), "text/html", from, until, format)
}
