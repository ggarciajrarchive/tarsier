package inf.eureka.tarsier.rest

import net.liftweb.http.rest.RestHelper
import net.liftweb.http.{JsonResponse, LiftResponse, GetRequest, Req}
import inf.eureka.tarsier.model.{User, Tag}
import net.liftweb.mapper.By
import net.liftweb.json.JsonDSL._

/**
 * User: colt44
 * Date: 10/21/12
 * Time: 3:23 PM
 */
object TagsRest extends RestHelper {
  serve({
    case Req("api" :: "tags" :: _, "json", GetRequest) => listTags()
  })

  def listTags(): LiftResponse = {
    val tags = ("tags" ->
      Tag.findAll(By(Tag.user, User.currentUser)).map(tg => {
        ("tag" -> tg.tag.get)
      })
      )

    JsonResponse.apply(tags)
  }
}
