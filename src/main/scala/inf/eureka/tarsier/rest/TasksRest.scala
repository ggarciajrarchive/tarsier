package inf.eureka.tarsier.rest

import net.liftweb.http.rest.RestHelper
import net.liftweb.http._
import net.liftweb.json.JsonAST._
import net.liftweb.json.JsonDSL._
import inf.eureka.tarsier.model.{TaskTags, Tag, User, Task}
import net.liftweb.common.{Box, Empty}
import net.liftweb.mapper.By
import inf.eureka.tarsier.lib.Formatter._
import inf.eureka.tarsier.lib.Trasformer._
import net.liftweb.common.Full

/**
 * User: colt44
 * Date: 8/18/12
 * Time: 5:33 PM
 */
case class TagJson(tag: String)

case class TaskJson(task: String,
                    start: Option[String],
                    end: Option[String],
                    tags: List[TagJson])

case class EditTaskJson(id: String,
                        task: String,
                        start: Option[String],
                        end: Option[String],
                        tags: List[TagJson])

case class DayJson(day: Long, tasks: List[TaskJson])

case class IdJson(id: Long)

object TasksRest extends RestHelper {
  serve("api" / "tasks" prefix {
    case Req(start :: end :: "list" :: _, "json", GetRequest) =>
      listTasks(start, end)

    case Req(id :: "delete" :: _, "json", GetRequest) =>
      deleteTask(id)

    case _ JsonPost json -> _ => createTask(json)

    case _ JsonPut json -> _ => editTask(json)
  })

  private def task2json(t: Task) = {
    ("task" -> t.content.get) ~
      ("id" -> t.id.get) ~
      ("start" -> format(t.start, "dd/MM/yyyy HH:mm:ss")) ~
      ("end" -> format(t.end, "dd/MM/yyyy HH:mm:ss")) ~
      ("tags" -> t.tags.map(tg => {
        ("tag" -> tg.tag.get)
      }))
  }

  private def list2json(tasks: List[Task]) = {
    ("days" -> list2map(tasks).map(d => {
      ("day" -> d._1) ~ ("tasks" -> d._2.map(t => task2json(t)))
    }))
  }

  def listTasks(s: String, e: String) = {
    def listByPeriod = Task.listByPeriod(
      parse(Full(s + " 00:00:00"), "dd-MM-yyyy HH:mm:ss"),
      parse(Full(e + " 00:00:00"), "dd-MM-yyyy HH:mm:ss")
    )

    val tasks = S.param("tags") match {
      case Full(p) if p.length > 0 => {
        Task.listByPeriodAndTags(
          parse(Full(s + " 00:00:00"), "dd-MM-yyyy HH:mm:ss"),
          parse(Full(e + " 00:00:00"), "dd-MM-yyyy HH:mm:ss"),
          p.split(",").map(t => "'" + t + "'").mkString(",")
        )
      }
      case Full(t) => listByPeriod
      case _ => listByPeriod
    }

    JsonResponse.apply(list2json(tasks))
  }

  def createTask(j: JValue) = {
    implicit val formats = net.liftweb.json.DefaultFormats

    var lastRunning: Box[Task] = Empty
    val t = j.extract[TaskJson]

    def newTask = {
      Task.getRunning match {
        case Full(tk) => {
          tk.end(parse(t.start, "dd/MM/yyyy HH:mm")).save

          lastRunning = Full(tk)
        }
        case _ => // Do nothing
      }

      Task.create.
        user(User.currentUser).
        content(t.task).
        start(parse(t.start, "dd/MM/yyyy HH:mm")).
        saveMe()
    }

    def oldTask = {
      Task.create.
        user(User.currentUser).
        content(t.task).
        start(parse(t.start, "dd/MM/yyyy HH:mm")).
        end(parse(t.end, "dd/MM/yyyy HH:mm")).
        saveMe()
    }

    val task = if (!t.start.isEmpty && !t.end.isEmpty) oldTask else newTask

    t.tags.map(t => {
      val token = t.tag.substring(1)
      val tag = Tag.find(By(Tag.tag, token), By(Tag.user, User.currentUser)) match {
        case Full(tg) => Full(tg)
        case _ => Full(Tag.create.tag(token).user(User.currentUser).saveMe())
      }

      TaskTags.create.
        tag(tag).
        task(task).
        save()
    })

    val lr = lastRunning match {
      case Full(tlr) => task2json(tlr)
      case _ => JNothing
    }

    ("lastRunning" -> lr) ~ ("lastAdded" -> task2json(task))
  }

  def editTask(j: JValue) = {
    implicit val formats = net.liftweb.json.DefaultFormats

    val t = j.extract[EditTaskJson]

    Task.find(By(Task.id, t.id.toLong), By(Task.user, User.currentUser)) match {
      case Full(task) => {
        val end = if (t.end.isEmpty) null else parse(t.end, "dd/MM/yyyy HH:mm")
        task.content(t.task).
          start(parse(t.start, "dd/MM/yyyy HH:mm")).
          end(end).save

        TaskTags.deleteByTask(task)

        t.tags.map(t => {
          val token = t.tag.substring(1)
          val tag = Tag.find(By(Tag.tag, token)) match {
            case Full(tg) => Full(tg)
            case _ => Full(Tag.create.tag(token).user(User.currentUser).saveMe())
          }

          TaskTags.create.
            tag(tag).
            task(task).
            save()
        })
      }
      case _ => // Do nothing
    }

    j
  }

  def deleteTask(j: String) = {
    val t = Task.find(By(Task.id, j.toLong), By(Task.user, User.currentUser)) match {
      case Full(task) => {
        task.delete_!
        TaskTags.deleteByTask(task)
        task2json(task)
      }
      case _ => JNothing
    }

    JsonResponse.apply(t)
  }
}
